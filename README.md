## Base64 with optional modified alphabet

-------------------

I've use this script in analyzing threats or malwares that uses modified alphabet.



```
usage: Base64custom.py [-h] -in INPUT_DATA [-out FILE_OUT] [-c CUSTOMALPHABET]
                       [-e] [-d]

Decode or encode base64 with an optional custom alphabet

optional arguments:
  -h, --help            show this help message and exit
  -in INPUT_DATA, --input_data INPUT_DATA
                        Either string or file input to be encoded
  -out FILE_OUT, --file_out FILE_OUT
                        File where to save the decoded output, else output
                        will be printed on console
  -c CUSTOMALPHABET, --customalphabet CUSTOMALPHABET
                        Specify a custom alphabet
  -e, --encode          Encode data
  -d, --decode          Decode data
```


* Note: the modified alphabet (CUSTOMALPHABET) should be retrieved by reversing the target malware, else will use the standard alphabet of 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

---

