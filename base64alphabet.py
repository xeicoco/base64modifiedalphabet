import argparse
import os


standard_alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' #standard base64 alphabet


def decode_func(input_data, alphabet, output_data):

    t = [-1] * 256
    _alpha = [ord(ix) for ix in alphabet]

    for i in range(64):
        t[_alpha[i]] = i


    _in = input_data
    if os.path.isfile(input_data):
        _h = open(input_data, "rb")
        _in = _h.read()
        _h.close()

    val = 0
    valb = -8
    outs = ""

    for _d in _in:
        c = ord(chr(_d))

        if t[c] == -1: 
            break

        val = (val << 6) + t[c]
        valb += 6

        if (valb>=0):
            outs += chr((val>>valb)&0xFF)
            valb-=8

    return outs

def encode_func(input_data, alphabet, output_data):

    _in = input_data
    if os.path.isfile(input_data):
        _h = open(input_data, "rb")
        _in = _h.read()
        _h.close()

    val = 0
    valb = -6
    outs = ""

    for _d in _in:
        c = ord(chr(_d))
        val = (val << 8) + c
        valb += 8

        while(valb>=0):
            outs += alphabet[(val>>valb)&0x3F]
            valb -= 6

    if (valb>-6):
        outs += alphabet[((val<<8)>>(valb+8))&0x3F]

    mod = (len(outs) % 4)
    outs += "=" * (mod > 0 if (4 - mod) else mod )
    return outs





def main():
	parser = argparse.ArgumentParser(description='Decode or encode base64 with an optional custom alphabet')
	parser.add_argument('-in','--input_data',help='Either string or file input to be encoded',required=True)
	parser.add_argument('-out','--file_out',help='File where to save the decoded output, else output will be printed on console',required=False)
	parser.add_argument('-c','--customalphabet',help='Specify a custom alphabet',required=False)
	parser.add_argument('-e','--encode',help='Encode data',action='store_true',required=False)
	parser.add_argument('-d','--decode',help='Decode data',action='store_true',required=False)

	args = parser.parse_args()

	input_data = args.input_data
	output_data = args.file_out

	encode = args.encode
	decode = args.decode
	alphabet = standard_alphabet

	if not encode and not decode:
	    print ('No action (encode or decode) specified...')
	    exit()
	elif encode and decode:
	    print ('One action at a time only...')
	    exit()




	if args.customalphabet:
	    alphabet = args.customalphabet
	    uniqset_standard = set(list(standard_alphabet))
	    uniqset_custom = set(list(alphabet))
	    if len(uniqset_custom) != len(uniqset_standard):
	        excess_set = uniqset_custom - uniqset_standard
	        if len(excess_set) > 0:
	            print ('Invalid custom alphabet, either there\'s a duplicate character, insuficient character set or invalid char...')
	            exit()


	codecoded = ""

	if decode:
	    codecoded = decode_func(input_data, alphabet, output_data)
	    print ('Decoded output:')

	elif encode:
	    codecoded = encode_func(input_data, alphabet, output_data)
	    print ('Encoded output:')

	if (args.file_out != None):
	    fo = open(output_data, "wb")
	    for ix in codecoded:
	        fo.write(bytes([ord(ix)]))
	    fo.close()
	else:
	    print(codecoded)



if __name__ == '__main__':
    main()


